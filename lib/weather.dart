import 'package:flutter/material.dart';

void main() {
  runApp(Weather());
}

class Weather extends StatefulWidget{
  @override
  State<Weather> createState() => _WeatherState();
}

class _WeatherState extends State<Weather> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: null,
        body: buildBodyWidget(),
        // floatingActionButton: FloatingActionButton(
        //   child : Icon(Icons.threesixty),
        //   onPressed: () {
        //     setState(() {
        //       currentTheme == APP_THEME.DARK
        //           ? currentTheme = APP_THEME.LIGHT
        //           : currentTheme = APP_THEME.DARK;
        //     });
        //   },
        // ),
      ),
    );
  }
}

Widget buildWeather1() {
  return Column(
    children: <Widget>[
      Text("12:00"),
      Text(""),
      Icon(
        Icons.sunny,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather2() {
  return Column(
    children: <Widget>[
      Text("13:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather3() {
  return Column(
    children: <Widget>[
      Text("14:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather4() {
  return Column(
    children: <Widget>[
      Text("15:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather5() {
  return Column(
    children: <Widget>[
      Text("16:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather6() {
  return Column(
    children: <Widget>[
      Text("17:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}

//ListTile
Widget TodayListTile() {
  return ListTile(
    title: Text("Today"),
    subtitle: Text("27° / 36°"),
    trailing: Icon(Icons.thunderstorm),
  );
}
Widget TomorrowListTile() {
  return ListTile(
    title: Text("Tomorrow"),
    subtitle: Text("27° / 35°"),
    trailing: Icon(Icons.sunny),
  );
}

// AppBar buildAppBarWidget() {
//   return AppBar(
//     // backgroundColor: Colors.white,
//     leading: IconButton(
//       icon: Icon(Icons.menu),
//       // color: Colors.black,
//       onPressed: (){
//         print("Menu");
//       },
//     ),
//     actions: <Widget>[
//       IconButton(
//         icon: Icon(Icons.settings),
//         // color: Colors.black,
//         onPressed: (){
//           print("Settings");
//         },
//       ),
//     ],
//   );
// }

Widget buildBodyWidget() {
  return Container(
    decoration: const BoxDecoration(
      image: DecorationImage(
        image: NetworkImage(
          "https://images.unsplash.com/photo-1593978301851-40c1849d47d4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8d2VhdGhlciUyMGFwcHxlbnwwfHwwfHw%3D&w=1000&q=80",
        ),
        fit: BoxFit.cover,
      ),
    ),
    child: ListView (
        children: <Widget>[
          Column(
            children: <Widget>[
              // Container(
              //   width: double.infinity,
              //   //Height constraint at Container widget level
              //   height: 400,
              //   child: Image.network(
              //     "https://i.imgur.com/LDG1v9c.jpg",
              //     fit: BoxFit.fill,
              //   ),
              // ),
              SizedBox(height: 50),
              Center(
                child: Column(
                  children: [
                    Text(" 32°",
                        style: TextStyle(fontSize: 100)
                    ),
                    Text("Cloudy",
                        style: TextStyle(fontSize: 40)
                    ),
                    Text("Real 35°",
                        style: TextStyle(fontSize: 26)
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50),
              Container(
                margin: const EdgeInsets.all(8),
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.blue.withOpacity(0.8),
                  border: Border.all(
                    style: BorderStyle.none
                  ),
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                child : profileActionItems(),
              ),
              Container(
                margin: const EdgeInsets.all(8),
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.blue.withOpacity(0.8),
                    border: Border.all(
                        style: BorderStyle.none
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                child : Column(
                  children: [
                    TodayListTile(),
                    TomorrowListTile(),
                  ],
                ),
              ),

            ],
          )
        ]
    ),
  );
}
Widget profileActionItems() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildWeather1(),
        buildWeather2(),
        buildWeather3(),
        buildWeather4(),
        buildWeather5(),
        buildWeather6(),
      ]
  );
}
